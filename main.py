from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

endpoint = 'https://google.com'

def main():
	driver = webdriver.Chrome()
	driver.implicitly_wait(5)
	driver.get(endpoint)
	time.sleep(2)
	element= driver.find_element_by_xpath("//*[@aria-label='検索']")
	element.send_keys("twitter クモクモくん公式")
	time.sleep(2)
	element.send_keys(Keys.ENTER)
	
	driver.find_element_by_xpath("//a[@href='https://twitter.com/scrapinglab_k']").click()
	time.sleep(3)
	driver.quit()
if __name__ == '__main__':
	main()
